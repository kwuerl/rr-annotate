const path = require('path');

module.exports = {
    entry: './src/index.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    output: {
        library: "index",
        libraryTarget: "umd",
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist')
    },
    externals: {
        "reflect-metadata": "reflect-metadata"
    }
};