import {Reducer} from "redux";
import 'reflect-metadata';

const actionMetaKey = Symbol("actionMetaKey");
const actionDefaultMetaKey = Symbol("actionDefaultMetaKey");

export function BindAction<ActionInterface>(actionClass: { new(...params: any[]): ActionInterface & {type: string}} ) {
    return function (
        target: {initialState: any},
        propertyKey: string,
        descriptor: TypedPropertyDescriptor<(state:any, action:ActionInterface) => any>
    ) {
        let instance = new actionClass();
        let exisintMeta: string[] = Reflect.getMetadata(actionMetaKey, target, propertyKey);
        if(!exisintMeta) {
            exisintMeta = [];
        }
        if(!exisintMeta.find(a => a === instance.type))
            exisintMeta.push(instance.type);
        Reflect.defineMetadata(actionMetaKey, exisintMeta, target, propertyKey);
    }
}
export function DefaultAction() {
    return function (
        target: Object,
        propertyKey: string,
        descriptor: TypedPropertyDescriptor<(state:any) => any>
    ) {
        return Reflect.defineMetadata(actionDefaultMetaKey, true, target, propertyKey);
    }
}

export interface ReducerOpt {
    useReference: boolean
}
export function exportBoundReducers<State>(type: { new(): {initialState:State} }, opts: ReducerOpt = { useReference: true }): Reducer<State> {
    let reducerObject = new type();
    let reduceClassProto = Object.getPrototypeOf(reducerObject);
    let reducerNames = Object.getOwnPropertyNames(reduceClassProto);
    const equals = opts.useReference ? ((current: string, action: string) => current === action) : ((current: string, action: string) => current == action);
    return (state: State | undefined, action: any) => {
        if(!state) state = reducerObject.initialState;
        let defaultReducer = null;
        for (let reducerName of reducerNames) {
            let actions: string[] = Reflect.getMetadata(actionMetaKey, reducerObject, reducerName) || [];
            let found = actions.find(a => equals(a, action.type));
            if (found) {
                let method = reduceClassProto[reducerName];
                return method(state, action);
            }
            if (Reflect.getMetadata(actionDefaultMetaKey, reducerObject, reducerName)) {
                defaultReducer = reduceClassProto[reducerName];
            }
        }
        return defaultReducer ? defaultReducer(state, action) : state;
    }
}
