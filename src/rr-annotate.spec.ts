import { expect } from 'chai'
import {BindAction, DefaultAction, exportBoundReducers} from "./rr-annotate";
import * as reflectionModule from 'reflect-metadata';
import {ImportMock} from "ts-mock-imports";
import {Action, createStore, Store} from "redux";

class BindableAction {
    public readonly type = "ACTION_TYPE";
    constructor(public param: number) {};
}

class SimpleBindableAction {
    type = "SIMPLE_TYPE";
    constructor() {};
}

class OtherSimpleBindableAction {
    type = "OTHER_SIMPLE_TYPE";
    constructor() {};
}

interface OtherAction extends Action<'OTHER_ACTION'> {
    param: number;
}

var foo:OtherAction = <any>{};

export const initialSystemState = {
    called: "initial"
};

class Reducers {
    readonly initialState: any = {called: "initial"};

    @BindAction(BindableAction)
    reducerMethod(state: {}, action: {param: number}) : any {
        return {called: "reducerMethod"};
    }

    @BindAction(SimpleBindableAction)
    @BindAction(SimpleBindableAction)
    reducerMethodMultiOne(state: {}) : any {
        return {called: "reducerMethodMultiOne"};
    }

    @BindAction(SimpleBindableAction)
    @BindAction(OtherSimpleBindableAction)
    reducerMethodMulti(state: {}) : any {
        return {called: "reducerMethodMulti"};
    }

    @DefaultAction()
    defaultReducer(state: {}) {
        return state;
    }
}

function readMetatadaKeys(propertyKey: string) {
    return Reflect.getMetadataKeys(Reducers.prototype, propertyKey);
}

function readMetadata(propertyKey: string)  {
    let keys = readMetatadaKeys(propertyKey);
    if(keys.length != 0) {
        return Reflect.getMetadata(keys[0], Reducers.prototype, propertyKey);
    } else {
        return undefined;
    }
}

describe('BindActionSingle', () => {
    var value: any;
    var keys: any[];

    beforeEach('find keys', () => {
        value = readMetadata("reducerMethod");
        keys = readMetatadaKeys("reducerMethod");
    });

    it('should annotate a single key on reducerMethod', () => {
        expect(keys.length).to.equal(1);
    });

    it('should annotate with right value reducerMethod', () => {
        expect(value).to.eql(['ACTION_TYPE']);
    });
});

describe('BindActionMultiOverride', () => {
    var value: any;

    beforeEach('find keys', () => {
        value = readMetadata("reducerMethodMultiOne");
    });

    it('should annotate with right value reducerMethodMultiOne', () => {
        expect(value).to.eql(['SIMPLE_TYPE']);
    });

});

describe('BindActionMulti', () => {
    var value: any;

    beforeEach('find keys', () => {
        value = readMetadata("reducerMethodMulti");
    });

    it('should annotate with right value reducerMethodMultiOne', () => {
        expect(value).to.have.members(['SIMPLE_TYPE', 'OTHER_SIMPLE_TYPE']);
    });
});

describe('exportBoundReducers', () => {
    var reducer: (state: {} | undefined, action: any) => {};
    var store: Store;
    beforeEach(() => {
        reducer = exportBoundReducers<{}>(Reducers);
        store = createStore(reducer);
    });

    it('should use internal state and default reducer', () => {
        store.dispatch({type: ""});
        expect(store.getState()).to.eql({called: "initial"});
    });
    it('should call the right reducer with input action BindableAction', () => {
        store.dispatch({...new BindableAction(0)});
        expect(store.getState()).to.eql({called: "reducerMethod"});
    });
    it('should call the right reducer with input action SimpleBindableAction', () => {
        store.dispatch( {...new SimpleBindableAction()});
        expect(store.getState()).to.eql({called: "reducerMethodMultiOne"});
    });
    it('should call the right reducer with input action OtherSimpleBindableAction', () => {
        store.dispatch({...new OtherSimpleBindableAction()});
        expect(store.getState()).to.eql({called: "reducerMethodMulti"});
    });
    it('should call defaultReducer', () => {
        let state = reducer({called: "defaultReducer"}, {});
        expect(state).to.eql({called: "defaultReducer"});
    });
});